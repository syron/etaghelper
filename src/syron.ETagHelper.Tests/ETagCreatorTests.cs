﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace syron.ETagHelper.Tests
{
    [TestClass]
    public class ETagCreatorTests
    {
        [TestMethod]
        public void GenerateEtag()
        {
            string testString = "Hello World";
            var etag1 = ETagCreator.Create(testString);
            var etag2 = ETagCreator.Create("Hello World");

            Assert.AreEqual(etag1, etag2);
        }
    }
}
