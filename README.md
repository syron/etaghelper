# ETagHelper

Helps you to easily create an ETag for an object.

Use:
`ETagCreator.Create("Hello World");`

[![Build status](https://ci.appveyor.com/api/projects/status/ru53tuubcgu167q3?svg=true)](https://ci.appveyor.com/project/syron/etaghelper)